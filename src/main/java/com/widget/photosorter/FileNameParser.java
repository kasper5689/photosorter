package com.widget.photosorter;

public class FileNameParser {
    public static final int ANTON_PROFILE=0;
    public static final int LYUDA_PROFILE=1;

    public static void parse(FileData file){
        int profile;
        String fileName = file.getOldPlace().getName();
        profile = identifyProfile(fileName);
        if (profile == ANTON_PROFILE){
            file.setProfile(ANTON_PROFILE);
            file.setYear(fileName.substring(4,8));
            file.setMounth(fileName.substring(8,10));
            file.setDayInMounth(fileName.substring(10,12));
        }else if (profile == LYUDA_PROFILE){
            file.setProfile(LYUDA_PROFILE);
            file.setYear(fileName.substring(3,7));
            file.setMounth(fileName.substring(7,9));
            file.setDayInMounth(fileName.substring(9,11));
        }else {
            System.out.println("Unknown file name profile: " + fileName);
        }

    }

    private static int identifyProfile(String name){
        int profile = -1;
        if (name.startsWith("IMG") || name.startsWith("VID")) {
            profile = ANTON_PROFILE;
        }else if (name.startsWith("WP")){
            profile = LYUDA_PROFILE;
        }
        return profile;
    }
}
