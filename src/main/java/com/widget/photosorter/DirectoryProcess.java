package com.widget.photosorter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class DirectoryProcess {
    private List<FileData> process = new ArrayList<>();

    public void prepareProcess(String inputDirectory){
        System.out.println("Processing folder: "+ inputDirectory);
        try {
            Files.list(Paths.get(inputDirectory)).forEach(file->{
                process.add(new FileData(file.toFile()));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        process.forEach(FileNameParser::parse);
    }

    public void moveFiles(String toDirectory, boolean useForkJoin){
        process.forEach(file -> file.setNewPlace(new File(toDirectory+"/"+file.getNewDirectory()+"/"+file.getOldPlace().getName())));
//        process.forEach(fileData -> fileData.getOldPlace().renameTo(fileData.getNewPlace()));
        process.forEach(fileData -> {
            try {
                if (fileData.getProfile() != -1) {
                    File path = new File(fileData.getNewPlace().getParent());
                    path.mkdirs();
                    Files.move(fileData.getOldPlace().toPath(), fileData.getNewPlace().toPath());
                    System.out.println(fileData.getOldPlace().getAbsolutePath() + " -> " + fileData.getNewPlace().getAbsolutePath());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
