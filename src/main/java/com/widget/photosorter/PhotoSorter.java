package com.widget.photosorter;

import java.lang.reflect.Array;
import java.util.Arrays;

public class PhotoSorter {
    static Config config;
    public static void main(String [] args){
        Arrays.asList(args).forEach(arg->{
            System.out.println(arg);
        });
        config = new Config();
        config.parseArguments(args);
        DirectoryProcess process = new DirectoryProcess();
        process.prepareProcess(config.getInputFolder());
        process.moveFiles(config.getOutputFolder(),config.isUseForkJoin());
    }
}
