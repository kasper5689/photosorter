package com.widget.photosorter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FileData {
    private File oldPlace;
    private File newPlace;

    private String year;
    private String mounth;
    private String dayInMounth;

    private int profile = -1;

    private static Map<String,String> mounthMap= new HashMap<>();
    static {
        mounthMap.put("01", "Январь");
        mounthMap.put("02", "Февраль");
        mounthMap.put("03", "Март");
        mounthMap.put("04", "Апрель");
        mounthMap.put("05", "Май");
        mounthMap.put("06", "Июнь");
        mounthMap.put("07", "Июль");
        mounthMap.put("08", "Август");
        mounthMap.put("09", "Сентябрь");
        mounthMap.put("10", "Октябрь");
        mounthMap.put("11", "Ноябрь");
        mounthMap.put("12", "Декабрь");
    }

    public FileData(File initialFile) {
        this.oldPlace = initialFile;
    }

    public String getNewDirectory(){
        return "("+mounth+") "+mounthMap.get(mounth)+" "+ dayInMounth;
    }

    public File getOldPlace() {
        return oldPlace;
    }

    public File getNewPlace() {
        return newPlace;
    }

    public void setNewPlace(File newPlace) {
        this.newPlace = newPlace;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public void setDayInMounth(String dayInMounth) {
        this.dayInMounth = dayInMounth;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }
}
