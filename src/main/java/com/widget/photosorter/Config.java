package com.widget.photosorter;

import org.apache.commons.cli.*;

public class Config {

    private boolean useForkJoin = false;
    private String inputFolder;
    private String outputFolder;

    public boolean isUseForkJoin() {
        return useForkJoin;
    }

    public String getInputFolder() {
        return inputFolder;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public void parseArguments(String [] args){
        Options options = new Options();

        Option useForkJoinOpt = new Option("fj", "forkjoin", false,"use fork join for rename");
        useForkJoinOpt.setRequired(false);
        options.addOption(useForkJoinOpt);

        Option inputDirectory = new Option("i", "input", true,"input directiry for parsing. Default currcent folder");
        useForkJoinOpt.setRequired(false);
        options.addOption(inputDirectory);

        Option outptDirectory = new Option("o", "output", true,"filles will be moved to this dirctory with creating new structure. Default currcent folder");
        useForkJoinOpt.setRequired(false);
        options.addOption(outptDirectory);

        Option help = new Option("h", "help", false,"print usage");
        useForkJoinOpt.setRequired(false);
        options.addOption(help);

        CommandLineParser parser = new GnuParser();
        HelpFormatter formater = new HelpFormatter();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formater.printHelp("PhotoSorter", options);

            System.exit(1);
            return;
        }

        if (cmd.hasOption("help")) {
            formater.printHelp("PhotoSorter", options);
        }

        inputFolder = cmd.getOptionValue("input","./");
        outputFolder = cmd.getOptionValue("output","./");

        useForkJoin = cmd.hasOption("forkjoin");

        System.out.println("Programm arguments:");
        System.out.println("input folder: "+ inputFolder);
        System.out.println("output folder: "+ outputFolder);
        System.out.println("use fork joun: "+ useForkJoin);

    }
}
